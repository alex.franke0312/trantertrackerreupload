#include <Arduino.h>

#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#define USE_SERIAL Serial

WiFiMulti wifiMulti;

#include <TinyGPS++.h>
#include <axp20x.h>

TinyGPSPlus gps;
HardwareSerial GPS(1);
AXP20X_Class axp;

// Define LED outs
#define LEDONE 14
#define LEDTWO 13
#define LEDTHREE 2
#define LEDFOUR 23

// Define BUTTON ins
#define BUTTONONE 15
#define BUTTONTWO 35
#define BUTTONTHREE 32
#define BUTTONFOUR 33

int deliveryStatus;
String ngrokText;

void setup() {
  USE_SERIAL.begin(115200);

  Wire.begin(21, 22);
  if (!axp.begin(Wire, AXP192_SLAVE_ADDRESS)) {
    Serial.println("AXP192 Begin PASS");
  } else {
    Serial.println("AXP192 Begin FAIL");
  }
  axp.setPowerOutPut(AXP192_LDO2, AXP202_ON);
  axp.setPowerOutPut(AXP192_LDO3, AXP202_ON);
  axp.setPowerOutPut(AXP192_DCDC2, AXP202_ON);
  axp.setPowerOutPut(AXP192_EXTEN, AXP202_ON);
  axp.setPowerOutPut(AXP192_DCDC1, AXP202_ON);
  GPS.begin(9600, SERIAL_8N1, 34, 12);   //17-TX 18-RX

  for (uint8_t t = 4; t > 0; t--) {
    USE_SERIAL.printf("[SETUP] WAIT %d...\n", t);
    USE_SERIAL.flush();
    delay(1000);
  }

  //wifiMulti.addAP("Ziggo6531618", "Fq8rqrpykknp");
  wifiMulti.addAP("TranterTrackerHS", "TTHSsetup123");

  ngrokText = "http://9957-2001-1c04-3c18-de00-e87e-7049-f2be-6a3b.ngrok.io";

  //Configure button pinmodes
  pinMode(BUTTONONE, INPUT);
  pinMode(BUTTONTWO, INPUT);
  pinMode(BUTTONTHREE, INPUT);
  pinMode(BUTTONFOUR, INPUT);

  //Configure led pinmodes
  pinMode(LEDONE, OUTPUT);
  pinMode(LEDTWO, OUTPUT);
  pinMode(LEDTHREE, OUTPUT);
  pinMode(LEDFOUR, OUTPUT);

  deliveryStatus = 0;
}

void loop() {
  // wait for WiFi connection
  if ((wifiMulti.run() == WL_CONNECTED)) {

    HTTPClient http;

    USE_SERIAL.print("[HTTP] begin...\n");
    // configure traged server and url
    double latValue = (gps.location.lat() * 1000);
    double lngValue = (gps.location.lng() * 1000);
    String url = ngrokText + String("/update_db.php/?latitude=") + latValue + String("&longitude=") + lngValue + String("&deliveryStatus=") + deliveryStatus;
    USE_SERIAL.println(url);
    http.begin(url); //HTTP

    USE_SERIAL.print("[HTTP] GET...\n");
    // start connection and send HTTP header
    int httpCode = http.GET();

    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      USE_SERIAL.printf("[HTTP] GET... code: %d\n", httpCode);

      // file found at server
      if (httpCode == HTTP_CODE_OK) {
        String payload = http.getString();
        USE_SERIAL.println(payload);
      }
    } else {
      USE_SERIAL.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
  }

  smartDelay(2000);

  if (digitalRead(BUTTONONE) == HIGH)
    deliveryStatus = 0;
  if (digitalRead(BUTTONTWO) == HIGH)
    deliveryStatus = 1;
  if (digitalRead(BUTTONTHREE) == HIGH)
    deliveryStatus = 2;
  if (digitalRead(BUTTONFOUR) == HIGH)
    deliveryStatus = 3;

  if (deliveryStatus == 0) {
    digitalWrite(LEDONE, HIGH);
    digitalWrite(LEDTWO, LOW);
    digitalWrite(LEDTHREE, LOW);
    digitalWrite(LEDFOUR, LOW);
  }
  else if (deliveryStatus == 1) {
    digitalWrite(LEDONE, LOW);
    digitalWrite(LEDTWO, HIGH);
    digitalWrite(LEDTHREE, LOW);
    digitalWrite(LEDFOUR, LOW);
  }
  else if (deliveryStatus == 2) {
    digitalWrite(LEDONE, LOW);
    digitalWrite(LEDTWO, LOW);
    digitalWrite(LEDTHREE, HIGH);
    digitalWrite(LEDFOUR, LOW);
  }
  else if (deliveryStatus == 3) {
    digitalWrite(LEDONE, LOW);
    digitalWrite(LEDTWO, LOW);
    digitalWrite(LEDTHREE, LOW);
    digitalWrite(LEDFOUR, HIGH);
  }
}

static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    while (GPS.available())
      gps.encode(GPS.read());
  } while (millis() - start < ms);
}
