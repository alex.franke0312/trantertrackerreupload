

<!DOCTYPE html>
<html>

<head>
    <title>TripTracker V.0.1.6</title>
    <link rel="icon" type="image/x-icon" href="images/favicon.ico">
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css">

</head>
<style>
    :root {
        font-family: Arial, Helvetica, sans-serif;
        --placeholderImg: <img src="TempCarIcon.png"alt="Car icon sideview"style="width:70px";
    }

    table {
        border-collapse: collapse;
        width: 750px;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }

    #map {
        width: 100%;
        height: 50vh;
    }
</style>

<body>
    <h1>TripTracker</h1>
    By Alexander Franke. <a href="mailto:alex.franke0312@gmail.com">E-Mail</a>. <br>
    Version: V.0.1.6<br>
    Massive work-in-progress.

    <h2>Fleet status</h2>
    <div id="RestInfo">

    </div>

    <div id="Table">

    </div>
    <br>

    <div id="map"></div>

    <script>
        var rest_pos_x = 52.69169998168945; //TODO: Make these fetchable from DB
        var rest_pos_y = 5.24675989151001;

        fetch('http://localhost/get_rest_db.php')
            .then(response => response.json())
            .then(restaurantData => processRestaurantData(restaurantData));
        
        fetch('http://localhost/get_vic_db.php')
            .then(response => response.json())
            .then(vehicleData => processVehicleData(vehicleData));

        setInterval(function() {
            fetch('http://localhost/get_vic_db.php')
                .then(response => response.json())
                .then(vehicleData => processVehicleData(vehicleData));
        }, 5000); //TODO; make this an adjustable variable in the frontend        

        function deg2rad(coord) {
            return coord * Math.PI / 180;
        }

        function calculateDistanceKmLatLon(lat1, lat2, lon1, lon2) {
            //Converting to radians
            var longi1 = deg2rad(lon1);
            var longi2 = deg2rad(lon2);
            var lati1 = deg2rad(lat1);
            var lati2 = deg2rad(lat2);

            //Haversine Formula 
            var difflong = longi2 - longi1;
            var difflat = lati2 - lati1;

            var val = Math.pow(Math.sin(difflat / 2), 2) + Math.cos(lati1) * Math.cos(lati2) * Math.pow(Math.sin(difflong / 2), 2);

            //res1 = 3936 * (2 * asin(sqrt(val))); //for miles
            var res2 = 6378.8 * (2 * Math.asin(Math.sqrt(val))); //for kilometers

            //display distance in kilometers
            return ((res2).toFixed(2) + ' ' + ' Km');
        }

        const vehicleBrands = [];
        const vehicleModels = [];
        const vehicleStatus = [];
        const vehiclePosX = [];
        const vehiclePosY = [];

        function processVehicleData(vehicleData) {
            //TODO: automate the creation of arrays and names
            writeDataToArray(vehicleBrands, vehicleData, "vehicle_brand");
            writeDataToArray(vehicleModels, vehicleData, "vehicle_model");
            writeDataToArray(vehicleStatus, vehicleData, "vehicle_status");
            writeDataToArray(vehiclePosX, vehicleData, "vehicle_position_x");
            writeDataToArray(vehiclePosY, vehicleData, "vehicle_position_y");
            console.log("Processed all");

            var tableString = '<table id=\'Table\'><tr><th></th><th>Vehicle</th><th>Status</th><th>Distance from HQ</th></tr>'; // Header

            for (var j = 0; j < vehicleBrands.length; j++) {
                tableString += '<td style="width:75px;"><img src="images/TempCarIcon.png" alt="Car icon sideview" style="width:70px" ;></td>'; // Image
                tableString += '<td>' + vehicleBrands[j] + ' ' + vehicleModels[j] + '</td>'; // Vehicle
                tableString += '<td>' + vehicleStatus[j] + '</td>'; // Status
                tableString += '<td>' + calculateDistanceKmLatLon(vehiclePosX[j], rest_pos_x, vehiclePosY[j], rest_pos_y) + '</td>'; // Distance
                tableString += '</th></tr>'; // Closing
            }
            tableString += '</table>';

            var elem = document.getElementById('Table');

            elem.outerHTML = tableString;
        }

        function processRestaurantData(restData) {
            const restaurantName = [];
            const restaurantOwner = [];

            writeDataToArray(restaurantName, restData, "restaurant_name");
            writeDataToArray(restaurantOwner, restData, "restaurant_owner");

            var restInfoString = '<p id=\'RestInfo\'>Restaurant info: <strong>' + restaurantName[0] + '</strong>, o.l.v. <strong>' + restaurantOwner[0] + '</strong>.</p>'; // TODO: Make this work for a larger array of restaurants (not necessary for now).

            var elem = document.getElementById('RestInfo');

            elem.outerHTML = restInfoString;
        }

        function writeDataToArray(array, dataSet, dataCategory) {
            // TODO: automate creating arrays as well?

            // Parse all necessary info to arrays
            for (var i = 0; i < dataSet.length; i++) { // TODO: Remove empty slot from array (might be a encode issue in get_db)
                array[i] = dataSet[i][dataCategory];
            }
        }
    </script>
    
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"></script>

    <script>
        var map = L.map('map').setView([rest_pos_x, rest_pos_y], 13);

        var vehicleIcon = L.icon({
            iconSize: [138, 195]
        });

        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(map);

        L.marker([52.69165, 5.24675]).addTo(map)

        setInterval(() => {
            getPositionToLeafletMap(vehiclePosX[1], vehiclePosY[1]);
        }, 5000);

       var marker;

        function getPositionToLeafletMap(lat, lng){
            var lat = lat;
            var lng = lng;
            
            if(marker){
                map.removeLayer(marker);
            }

            marker = L.marker([lat, lng]).addTo(map)
        }
    </script>

</body>

</html>