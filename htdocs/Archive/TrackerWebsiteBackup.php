<!DOCTYPE html>
<html>

<head>
    <title>TripTracker V.0.1.2</title>
</head>
<style>
    :root {
        font-family: Arial, Helvetica, sans-serif;
    }

    table {
        border-collapse: collapse;
        width: 750px;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>

<body>

    <h1>TripTracker</h1>
    By Alexander Franke. <a href="mailto:alex.franke0312@gmail.com">E-Mail</a>. <br>
    Version: V.0.1.2<br>
    Massive work-in-progress.
    <h2>Fleet status</h2>
    <?php
    $servername = "localhost";
    $username = "mydatabase_admin";
    $password = "beheerbeer";
    $dbname = "my_database";
    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }
    $sql = "SELECT restaurant_position_x, restaurant_position_y FROM restaurants";
    $result = $conn->query($sql);
    $row = $result->fetch_assoc();
    $rest_pos_x = $row["restaurant_position_x"];
    $rest_pos_y = $row["restaurant_position_y"];
    ?>

    <table>
        <tr>
            <th></th>
            <th>Vehicle</th>
            <th>Status</th>
            <th>Distance from HQ</th>
        </tr>

        <?php
        $sql = "SELECT vehicle_numplate, vehicle_brand, vehicle_model, vehicle_status, vehicle_position_x, vehicle_position_y  FROM vehicle";
        $result = $conn->query($sql);

        function calculateDistanceKmLatLon($lat1, $lat2, $lon1, $lon2)
        {
            //Converting to radians
            $longi1 = deg2rad($lon1);
            $longi2 = deg2rad($lon2);
            $lati1 = deg2rad($lat1);
            $lati2 = deg2rad($lat2);

            //Haversine Formula 
            $difflong = $longi2 - $longi1;
            $difflat = $lati2 - $lati1;

            $val = pow(sin($difflat / 2), 2) + cos($lati1) * cos($lati2) * pow(sin($difflong / 2), 2);

            //$res1 = 3936 * (2 * asin(sqrt($val))); //for miles
            $res2 = 6378.8 * (2 * asin(sqrt($val))); //for kilometers

            //display distance in kilometers
            print(round($res2, 2) . ' ' . ' Km');
        }

        if ($result->num_rows > 0) {
            // output data of each row
            while ($row = $result->fetch_assoc()) {
                echo "<tr><br>";
                echo "<td style=\"width:75px;\"><img src=\"TempCarIcon.png\" alt=\"Car icon sideview\" style=\"width:70px\" ;></td>";
                echo "<td>", $row["vehicle_brand"], " ", $row["vehicle_model"], "</td>";
                echo "<td>", $row["vehicle_status"], "</td>";
                echo "<td>", calculateDistanceKmLatLon($row["vehicle_position_x"], $rest_pos_x, $row["vehicle_position_y"], $rest_pos_y), "</td>";
            }
        } else {
            echo "0 results";
        }
        ?>
    </table><br>

    <small><em>Imagine a very cool map being here, with live vehicle displays. Cool right! (Yeah that is WIP too)</em></small><br><br>

    <!-- Placeholder. This has to be a local, moveable map with moving positions derived from the GPS coordinates. 
    <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=5.133533477783204%2C52.645875678333496%2C5.360126495361329%2C52.736603469662796&amp;layer=mapnik&amp;marker=52.6912631403272%2C5.246829986572266" style="border: 1px solid black"></iframe><br />
    http://leafletjs.com/examples/quick-start.html USE THIS!!!!!
    -->

    <?php
    $conn->close();
    ?>

</body>

</html>