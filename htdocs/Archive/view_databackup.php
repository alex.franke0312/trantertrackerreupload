<html>

<head>
    <title>TripTracker V.0.1.2</title>
</head>
<style>
    :root {
        font-family: Arial, Helvetica, sans-serif;
    }

    table {
        border-collapse: collapse;
        width: 750px;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>

<body>
    <h1>TripTracker</h1>
    By Alexander Franke. <a href="mailto:alex.franke0312@gmail.com">E-Mail</a>. <br>
    Version: V.0.1.2<br>
    Massive work-in-progress.

    <h2>Fleet status</h2>
    <script>
        fetch('http://localhost/get_db.php')
            .then(response => response.json())
            .then(data => console.log(data));

        function calculateDistanceKmLatLon($lat1, $lat2, $lon1, $lon2) {
            //Converting to radians
            $longi1 = deg2rad($lon1);
            $longi2 = deg2rad($lon2);
            $lati1 = deg2rad($lat1);
            $lati2 = deg2rad($lat2);

            //Haversine Formula 
            $difflong = $longi2 - $longi1;
            $difflat = $lati2 - $lati1;

            $val = pow(sin($difflat / 2), 2) + cos($lati1) * cos($lati2) * pow(sin($difflong / 2), 2);

            //$res1 = 3936 * (2 * asin(sqrt($val))); //for miles
            $res2 = 6378.8 * (2 * asin(sqrt($val))); //for kilometers

            //display distance in kilometers
            print(round($res2, 2) + ' ' + ' Km');
        }
    </script>

    <table>
        <tr>
            <th></th>
            <th>Vehicle</th>
            <th>Status</th>
            <th>Distance from HQ</th>
        </tr>

        <?php
        echo "<tr><br>";
        echo "<td style=\"width:75px;\"><img src=\"TempCarIcon.png\" alt=\"Car icon sideview\" style=\"width:70px\" ;></td>";
        echo "<td>", $row["vehicle_brand"], " ", $row["vehicle_model"], "</td>";
        echo "<td>", $row["vehicle_status"], "</td>";
        echo "<td>", calculateDistanceKmLatLon($row["vehicle_position_x"], $rest_pos_x, $row["vehicle_position_y"], $rest_pos_y), "</td>";
        ?>
    </table><br>

</body>

</html>