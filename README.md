# Tranter Tracker
<strong>An IOT Project by Alexander Franke, <br>Student at the Amsterdam University of Applied Sciences.</strong>
<small>Contact information at the bottom of this page.</small>

<h2>What is the Tranter Tracker?</h2>
First off: The meaning of 'To Trant' comes from Middle English 'tranten', from or cognate with Middle Dutch 'tranten' ("to step, walk"), and loosely translates to traveling, which is basically what food delivery services are all about, right?
<br><br>
<strong>Besides a far-fetched name, the Tranter Tracker is a service consisting out of a webpage, and a set of mobile devices that run on 12v car power, that use internet to communicate with aforementioned webpage. The goal behind the Tranter Tracker is to allow employers in the food delivery service branch more insights into their delivery employees.</strong>

<h3>Why do we need a Tracker?</h3>
During my time as a food delivery employee, I noticed the complete isolation in communication between the restaurant in myself, with one exception; phone calls. Though effective, handling a phone while driving is dangerous and impractical. It's also often a delayed interaction, requiring a lot of conversation before information can be shared, for instance "<i>I have one delivery left before I return</i>". What if this was possible using a single button push?

<h3>What does it do?</h3>
In short, it's an efficient, non-obtrusive communicator. In order of importance;
<ol>
<li>The Tracker can communicate how many deliveries are left, before the driver is RTB (returning to base).
<li>The Tracker tracks distance from the dispatch or headquarters.
<li>The Tracker gives insights into the effectiveness of an employee by measuring data.
<li>The Tracker is an add-on to the process; it is not meant to interfere with existing workflows.
</ol>
This is all accessed and controlled with limited inputs, allowing for safe operation during driving.

<h2>Set-Up Guide</h2>
As the project is still in the prototyping phase, this might change rapidly from version to version. The current system is very unclean and needs strengthening, polishing, and automatic configuring before it can be deployed. Some elements can be substituted by other pieces of soft-/hardware, but listed here are the elements I used for the latest version of the Tranter Tracker.

<h4>Hardware</h4>
<ol>
<li><strong>LilyGo TTGO T-Beam</strong> (model: LILYGO-H267-Q217). This could be exchanged for a different piece of hardware, as long as it's on an ESP32 platform, with a GPS module (the T-Beam uses a uBlox NEO-M8N-0-10). Frequency of the LoRa module is not relevant.
<li>Micro-USB data cable.
<li>A phone with wireless hotspot available.
</ol>

<h4>Requirements</h4>
<ol>
<li><strong>MAMP</strong> / LAMP / XAMP (MAMP preferred, V5.0.4 or above). <a href="https://www.mamp.info/en/windows/">Download here</a>.
<li>Latest version of <strong>Ngrok</strong>. <a href="https://ngrok.com/">Download here</a>.
<li><strong>Arduino IDE</strong> V1.8.19 or above. <a href="https://www.arduino.cc/en/software">Download here</a>.
<li>An IDE that supports C++ (<strong>Visual Studio Code</strong> recommended, V1.65.2 or above. <a href="https://code.visualstudio.com/Download">Download here</a>).
<li>An updated browser.
</ol>

<h4>Dependencies & libraries</h4>
<ol>
<li>ESP32 Dev Module Board library, to be installed in your Arduino IDE preferences;<br>
<i>https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json</i>
<li><strong>TinyGPS</strong> Library for Arduino IDE, V13.0.0 or above.
<li><strong>TinyGPSPlus</strong> Library for Arduino IDE, V0.0.2 or above.
<li>USB to Serial Converter, <strong>CP2104 or CH9102F</strong>. <a href="https://www.tinytronics.nl/shop/en/drivers-for-usb-to-serial-converters">Download here</a>.
</ol>

<h3>Step-by Step set up</h3>
<ol>
<li>Open and start MAMP. Configure it like the example below.<br>
<img src="guideImages\db_setup_1.png"></img><br>
In short; your database should contain tables; 'restaurants' and 'vehicle', that include the columns listed. Enter a single restaurant and a single vehicle to begin with.<br>
<small>Note: Changing the names of the tables, columns, or database, means changing most of the .php files in htdocs, especially get_vic_db and get_rest_db. This is not set up to automatically do this.</small>
<li>Drag all the files in 'htdocs' from the repository into your 'htdocs' folder in MAMP (likely under C:\MAMP\htdocs). Opening your browser with MAMP running and entering 'http://localhost/TrackerWebsite.php' should show the main website.
<li>Launch ngrok (recommended on port 80), and copy the http forwarding adress. Save it for now.
<li>Set up your Arduino IDE by entering the ESP32 Dev Module JSON in your preferences, and installing, then selecting the 'ESP32 Dev Module' from the board drop-down selector.
<li>Download the TranterTracker.ino file from this repository, and load it into your IDE. Open it.
<li>Grab your phone, and activate the wireless hotspot feature. Enter the credentials for connecting to it into the TranterTracker.ino file, next to wifiMulti.addAP(<strong>"yourAdress", "yourPassword"</strong>);<br>
<small>You can test the device by connecting it to your router like your PC like normal, but for remote functionality, it must be connected to your phone.</small>
<li>A little further down in the .ino, enter the ngrok http forwarding link under String url = String("<strong>http://[...]ngrok.io</strong>/insert_db.php/?[...]")
<li>Connect the LilyGo TTGO T-Beam to your PC using the micro-USB data cable, and upload the code form the Arduino IDE.
<li>After uploading, connect unplug the USB data cable from the PC, and power it from somewhere else, likely the 12V outlet from your car using an adapter. 
<li>Open the 'http://localhost/TrackerWebsite.php' site, and check your results; when the GPS on the T-Beam is active (a red light is blinking), and your T-Beam is connected through the internet, your GPS data should automatically be updated every 5 seconds.
</ol>
You can use the serial monitor on baid 115200 after uploading to check whether the upload has completed succesfuly.

<h3>Contents of Repository</h3>
The repository consists out of 4 folders and a ReadMe file. The folders are;
<ul>
<li>GPSTester - A test .ino file for debugging the functionality of the GPS module.
<li>guideImages - Contains images for this very ReadMe file, nothing more.
<li>htdocs - Contains the website front- and backend files, to be ran by MAMP.
<li>TranterTracker - Contains the main .ino file for the LilyGo T-Beam.
</ul>

<h2>Contact Information</h2>
Alexander Franke<br>
University E-Mail: <a href="mailto:alex.franke@hva.nl">alex.franke@hva.nl</a><br>
Personal E-Mail: <a href="alex.franke0312@gmail.com">alex.franke0312@gmail.com</a><br>
Student No. 500792174<br>

<small>Last update: March 21<sup>st</sup>, 2022</small>