## Admin guide for TranterTracker
As the project is still in the prototyping phase, this might change rapidly from version to version. The current system is very unclean and needs strengthening, polishing, and automatic configuring before it can be deployed. Some elements can be substituted by other pieces of soft-/hardware, but listed here are the elements I used for the latest version of the Tranter Tracker.
<hr>

### Setting up the development space
<h4>Hardware</h4>
<ol>
<li><strong>LilyGo TTGO T-Beam</strong> (model: LILYGO-H267-Q217). This could be exchanged for a different piece of hardware, as long as it's on an ESP32 platform, with a GPS module (the T-Beam uses a uBlox NEO-M8N-0-10). Frequency of the LoRa module is not relevant.
<li>Micro-USB data cable.
<li>A phone with wireless hotspot available.
</ol>

<h4>Requirements</h4>
<ol>
<li><strong>The latest version of the TranterTracker.ino file in the repository.</strong> <a href="https://gitlab.fdmci.hva.nl/frankea2/trantertracker/">Download here</a>.
<li><strong>MAMP</strong> / LAMP / XAMP (MAMP preferred, V5.0.4 or above). <a href="https://www.mamp.info/en/windows/">Download here</a>.
<li>Latest version of <strong>Ngrok</strong>. <a href="https://ngrok.com/">Download here</a>.
<li><strong>Arduino IDE</strong> V1.8.19 or above. <a href="https://www.arduino.cc/en/software">Download here</a>.
<li>An IDE that supports C++ (<strong>Visual Studio Code</strong> recommended, V1.65.2 or above. <a href="https://code.visualstudio.com/Download">Download here</a>).
<li>An updated browser.
</ol>

<h4>Dependencies & libraries</h4>
<ol>
<li>ESP32 Dev Module Board library, to be installed in your Arduino IDE preferences;<br>
<i><a href=https://raw.githubusercontent.com/espressif/arduino-esp32/gh-pages/package_esp32_index.json>Click here to starts a download.</a></i>
<li><strong>TinyGPS</strong> Library for Arduino IDE, V13.0.0 or above.
<li><strong>TinyGPSPlus</strong> Library for Arduino IDE, V0.0.2 or above.
<li>USB to Serial Converter, <strong>CP2104 or CH9102F</strong>. <a href="https://www.tinytronics.nl/shop/en/drivers-for-usb-to-serial-converters">Download here</a>.
</ol>
<hr>
### Step-by-Step set up
<ol>
<li>Open and start MAMP, and head to your phpMyAdmin page by entering localhost/phpMyAdmin/ in your browser's URL bar. Configure it like the example later on this page.<br>
In short; your database should contain tables; 'restaurants' and 'vehicle', that include the columns listed. Enter a single restaurant and a single vehicle to begin with.<br>
<small>Note: Changing the names of the tables, columns, or database, means changing most of the .php files in htdocs, especially get_vic_db and get_rest_db. This is not set up to automatically do this.</small>
<li>Drag all the files in 'htdocs' from the repository into your 'htdocs' folder in MAMP (likely under C:\MAMP\htdocs). Opening your browser with MAMP running and entering 'http://localhost/TrackerWebsite.php' should show the main website.
<li>Launch ngrok (recommended on port 80), and copy the http forwarding adress. Save it for now.
<li>Set up your Arduino IDE by entering the ESP32 Dev Module JSON in your preferences, and installing, then selecting the 'ESP32 Dev Module' from the board drop-down selector.
<li>Download the TranterTracker.ino file from this repository, and load it into your IDE. Open it.
<li>Grab your phone, and activate the wireless hotspot feature. Enter the credentials for connecting to it into the TranterTracker.ino file, next to wifiMulti.addAP(<strong>"yourAdress", "yourPassword"</strong>);<br>
<small>You can test the device by connecting it to your router like your PC like normal, but for remote functionality, it must be connected to your phone.</small>
<li>A little further down in the .ino, enter the ngrok http forwarding link under String url = String("<strong>http://[...]ngrok.io</strong>/insert_db.php/?[...]")
<li>Connect the LilyGo TTGO T-Beam to your PC using the micro-USB data cable, and upload the code form the Arduino IDE.
<li>After uploading, connect unplug the USB data cable from the PC, and power it from somewhere else, likely the 12V outlet from your car using an adapter. 
<li>Open the 'http://localhost/TrackerWebsite.php' site, and check your results; when the GPS on the T-Beam is active (a red light is blinking), and your T-Beam is connected through the internet, your GPS data should automatically be updated every 5 seconds. <strong>It can take up to 10 minutes for the GPS connection to be established.</strong>
</ol>
You can use the serial monitor on baid 115200 after uploading to check whether the upload has completed succesfuly.<br><br>
<strong>Example for the Database:</strong>

![Database example](https://i.imgur.com/JwUugP1.png)