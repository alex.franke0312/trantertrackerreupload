## Technical Details
On this page you will find the Technical details on the TranterTracker's Embedded systems, and Web Application design. The layout and subjects of this page are decided upon by the requirements as ruled in the 2021 Study Manual of the Internet of Things Course by the Amsterdam University of Applied Sciences.
<hr>

### Architecture overview
This section is divided into three parts; the Embedded Architecture, the Web Application Architecture, and the Functional Design of the Web Application.

#### Embedded Architecture
Given that all components of the Embedded system are all included on the LilyGo TTGO T-Beam board, I will only include a short breakdown of how these are used.<br>

For starters, the SX1276 LoRa module of the T-Beam isn't used. That means the included LoRa antenna also isn't connected. The u-blox NEO-M8N GPS module is, however, and its voltage is controlled in the .ino code by using the AXP192 battery circuit controller. The built-in WiFi module in the <b>ESP32</b>-D-WD-Q6 microcontroller chip is the final element that is used, besides the microcontroller itself of course, which acts as a bridge between all the different modules. In the future, the bluetooth function could also be used. As a final note; 8 of the GPIO pins are used for in- and output of the buttons and LED's, respectively.

#### Architecture & Functional Design Web Application
![WebApp Function Diagram](https://imgur.com/x6fy4Hq.png)
<hr>

### Wiring diagram
![TranterTracker Wiring Diagram](https://imgur.com/TltDO6G.png)
<small>Note: The breadboard is not a necessary part of the system. Wires could (and should) be connected directly if possible.<br>
Though a shield or additional custom board is an option, I did not make or test one, so I will not show one either.</small>
<hr>

### Bill of materials
 <table>
  <tr>
    <th>Part Number</th>
    <th>Quantity</th>
    <th>Component</th>
    <th>Description</th>
    <th>Cost</th>
    <th>Part State</th>
    <th>Manufacturer</th>
    <th>Supplier</th>
    <th>Lead time</th>
    <th>SKU</th>
    <th>Supplier link</th>
  </tr>
  <tr>
    <td>0001-TBeam</td>
    <td>1</td>
    <td>LilyGO TTGO T-Beam</td>
    <td>LILYGO-H267-Q217</td>
    <td>€26,00</td>
    <td>In progress</td>
    <td>LilyGO</td>
    <td>Tinytronics.nl</td>
    <td>2 days</td>
    <td>003626</td>
    <td><a href="https://www.tinytronics.nl/shop/en/development-boards/microcontroller-boards/with-gps/lilygo-ttgo-t-beam-lora-433mhz-neo-m8n-gps-esp32">LilyGO TTGO T-Beam</a></td>
  </tr>
  <tr>
    <td>0010-Button</td>
    <td>4</td>
    <td>Tactile Pushbutton</td>
    <td>BBTACT, Momentary 2pin 6*6*5mm</td>
    <td>€0.15</td>
    <td>In progress</td>
    <td>Generic</td>
    <td>Tinytronics.nl</td>
    <td>2 days</td>
    <td>000181</td>
    <td><a href="https://www.tinytronics.nl/shop/en/switches/manual-switches/pcb-switches/breadboard-tactile-pushbutton-switch-momentary-2pin-6*6*5mm">Breadboard Tactile Pushbutton</a></td>
  </tr>
    <tr>
    <td>0011-ButtonPushcap</td>
    <td>4</td>
    <td>Button Cap for Pushbutton</td>
    <td>BCAP-12x12x7.3MM-BLUE</td>
    <td>€0.15</td>
    <td>In progress</td>
    <td>Generic</td>
    <td>Tinytronics.nl</td>
    <td>2 days</td>
    <td>003061</td>
    <td><a href="https://www.tinytronics.nl/shop/en/components/knobs,-caps-and-covers/button-cap-for-tactile-pushbutton-switch-momentary-12x12x7.3mm-blue">Button Cap for Pushbutton</a></td>
  </tr>
  <tr>
    <td>0012-LED</td>
    <td>4</td>
    <td>Red LED 3mm Diffused</td>
    <td>RLED3MM</td>
    <td>€0.10</td>
    <td>In progress</td>
    <td>Generic</td>
    <td>Tinytronics.nl</td>
    <td>2 days</td>
    <td>001133</td>
    <td><a href="https://www.tinytronics.nl/shop/en/components/leds/leds/red-led-3mm-diffused">Red LED</a></td>
  </tr>
    <tr>
    <td>0013-Resistor</td>
    <td>8</td>
    <td>220Ω resistor</td>
    <td>220Ω LED Series resistor</td>
    <td>€0.05</td>
    <td>In progress</td>
    <td>Generic</td>
    <td>Tinytronics.nl</td>
    <td>2 days</td>
    <td>000318</td>
    <td><a href="https://www.tinytronics.nl/shop/en/components/resistors/resistors/220%CF%89-resistor(led-series-resistor)">220Ω resistor</a></td>
  </tr>
  <tr>
    <td>0020-Case</td>
    <td>1</td>
    <td>LilyGO T-Beam Case</td>
    <td>3D Printed prototype case</td>
    <td>~€0.50</td>
    <td>In progress</td>
    <td>Homemade</td>
    <td>N.a.</td>
    <td>N.a.</td>
    <td>DX11</td>
    <td><a href="https://www.thingiverse.com/thing:4576325">Original case file (unaltered)</a></td>
  </tr>
</table> 
<hr>

### Physical design
Though a physical design has not been created yet, sketches have been made to visualise how the device could look. There are several noteworthy design choices to be mentioned as well. For instance, in the 3rd, I considered multiple mounting options because a car is constantly moving. The color of the LEDs was also important, because we don't want to impede the driver. The decision of a shield was made, to decrease production complexity. But on the other hand, the 'box' that houses the device is remarkably simple and slim, not only because I couldn't really decide on a more noteworthy design, but in an effort to keep the design adaptable for every model of vehicle (see the 1st image, mounting positions), keeping it as small as possible is a good idea; the end user should have the final say where it goes, and making the TranterTracker bigger makes more difficult to cram into some possible positions. 
<img alt="Design note 1" src="https://i.imgur.com/cctcaDX.jpg">
<img alt="Design note 2" src="https://i.imgur.com/5o5Tm1k.jpg">
<img alt="Design note 3" src="https://i.imgur.com/3WS0HZC.jpg">