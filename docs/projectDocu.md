# Development Process: IoT Course
This page will detail the development process of the Embedded Technology and the Web Application, as required by the AUAS Internet of Things curriculum.<br>
It is important to know that at this stage, I (A Franke) am partially resitting this course, and have prior knowledge of most subjects discussed during the course. Therefor some basic elements might be glossed over, while the issues encountered will be highlighted.
<hr>

## Embedded Technology
As mentioned, I had a general idea of what I wanted to create during the course. This was partialy due to aformentioned prior experience, but partialy due to my part-time job as a food delivery driver as well. The idea to track vehicles wasn't new either to my knowledge, but I was surprised when I discovered the cost of entry was quite substantial for a purpose-built system.<br>
### Preparation
Immediately during the first day of this course, I started planning out the necessary requirements of the product and skills to use or work on said requirements. This process is not too relevant to Embedded Technologies, but it can be summarized by saying that the problem statement helped formulate the Main Selling Points, which flowed into a Minimum Viable Product, which was used to create a MoSCoW analysis. From that MoSCoW analysis, I created a list of components I would need, which at the time still included LORA as a communication solution.<br><br>
Researching what I would need, resulted in the discussion whether LORA was a viable method of communication; from the course's DLO I learned that it was the most used long-range communication method for micro-electronics. Why not go with internet right away? It would make it work stand-alone.<br>
Alas, LORA turned out to be somewhat unreliable over distances past ~1km, especially when there isn't a direct line-of-sight (think buildings, which a delivery vehicle is constantly driving around). Hence Internet was chosen, which had to come from a mobile hotspot for now, but I did consider Bluetooth Tethering for some time, so I had to factor that into buying my microcontroller.<br>
### Getting the hardware working
As for what hardware I used, I went with the <strong>LilyGO TTGO T-Beam</strong> for several reasons;
<ol>
<li>It contained almost every possible communication method, so I could experiment easily.
<li>Its ESP32 platform meant that I knew how to work with it.
<li>It was one of the only microcontrollers with GPS embedded into it.
<li>It's highly regarded and documented online; crucial for getting to grips quickly.
</ol>
I encountered no issues installing drivers or other software needed for programming onto the T-Beam, and everything seemed to go smoothly, right up until I started messing around with the GPS module.<br><br>
The long story short is that <u>I had no prior experience working with GPS</u> outside conventional use in mobile phones, so when I couldn't get my connection working from my attic bedroom, I started to get worried. I used several GPS testing .ino example from the internet, and it took me a full day to finally conclude that I couldn't get it working. As a final resource, I ran the GPS testing example running for several hours while I got lunch.<br>
When I returned, it turned out the GPS module was in fact functioning, as the Serial Monitor was spitting out the exact coordinates my house is located at. All it needed was time to connect. I continued to experiment with this, to no great effect, and concluded that the device needs 5 to 10 minutes to establish a GPS signal after booting up. This was bad, for the usage demanded that it only needed seconds, a minute at most; delivery vehicles (whose 12v power outlet I was planning on using for power) get turned on and off every couple of minutes. This issue got solved during testing; when placed in a car's glovebox compartment, the GPS only needed <b>5 seconds</b> to establish a GPS signal. I took what I learned from the numerous examples used for testing the GPS module, and implemented it into the database synchronization code detailed under Web Application Development.<br><br>
<i>I will not be going into detail on programming the WiFi connection programming of the T-Beam, as I had done this several times before already, and encountered no issues.</i><br>
![Testing Set up 2](https://i.imgur.com/RLBpTsZ.jpg)
![Testing Set up 1](https://i.imgur.com/GGnReiJ.jpg)
<small>To eliminate a factor in testing, a powerbank was used for the first test.</small>

### Expanding the functionality
Before even running the first tests, I realized that this aparatus (now named the TranterTracker), had more potential. I had highlighted some of these already in my MoSCoW analysis, but with the Minimum Viable Product now done, and all <i>must's</i>, I could get to work on my first <i>should</i>; <b>delivery status.</b><br><br>
The idea of the delivery status is simple; often a deliverer gets multiple orders at once, to improve efficiency in just about every possible way. The team at 'headquarters' often don't know how far along a journey a deiverer might be, so could I create something simple to solve this?<br>
The answer is to simply pass a number along with the coordinates I was already sending. This was determined by a button pressed by the deliverer in the vehicle. If a deliverer has 2 orders remaining, he pressed the button labeled '2'. The current status would be communicated using simple red LED's to the driver, and a number on the Web Interface for the team back at headquarters. Nothing more; the idea behind the TranterTracker is not to make it too involved.<br><br>
The kit from the Internet of Things course luckily contained the wires, buttons, caps, leds, and resistors I needed, and I had prior knowledge of working with GPIO's, but I still needed a refresher. Luckily, the course's DLO had this a-plenty, and after soldering male header pins to my T-Beam (and 3D-printing a case for it), the prototyping could begin.<br><br>
![Prototype GPIO 1](https://i.imgur.com/1ouUdRB.jpg)
<small>Don't mind the pizza box; it was required for the prototyping.</small><br>

I couldn't tell you a whole lot about prototyping the GPIO even if I wanted to, because it can be summed up by the miracle of it working first try. My previous experience played a part in this, but the methodical pace of connecting one button first, then connecting the LED to it through software, then having that one button communicate through WiFi, and only then expanding it to 4 buttons did most of the heavy lifting. That being said; my code is <i>quite</i> ugly because the Switch cases I planned on using weren't behaving, so I did some very nasty if-else statements that I am not pleased with.

### Future of the Embedded System
There are so many things I would change if I had the time; from integrating the GPIO to a shield-like board, and making a custom case using OpenSCAD, to cleaning up my code and, in general, improving ease of use. All of these 'issues' are casualties of prototyping, and I take solace in knowing there is a lot of potential hidden in the product that I am yet to extract. The conclusion will hence be that I am happy with the results and the lessons learned along the way (especially the stupid ones), and yet irritated that I couldn't get more done.

<hr>

## Web Application Development
If you have read the Embedded Technology documentation above, you might be able to figure out that I am resitting this course, and therefore have some prior knowledge. This does, however, not apply to Web Application development; I have skipped this entirely last semester, and haven't touched HTML, CSS, PHP, or even SQL in years. This means that, unlike Embedded Technology, I had to start from scratch. This meant two things; <b>YouTube tutorials</b>, and <b>W3schools</b>.

### Getting the basics going
The previous paragraph contains a small caveat; I've had an 8 week long HTML course in my first year of middle school, which just so happened to go over setting up tables, which meant that I could get by creating a visual example pretty easily. Combined with the info from W3schools, this part was pretty easily, and wasn't what's notable about the process. What was notable, was the absolute onslaught that <b>getting AJAX to work</b>.<br><br>
Below is the first version of the Website frontend, but served purely as a visual example. The visuals wouldn't really change along the course, but the backend would change how it functions.
![Visual Example](https://imgur.com/jQjBbS0.jpg)

### I don't like AJAX
There is a video made by one of the coaches / teachers / legends of the Internet of Things course, Bas Pijls. The video is more than an hour long, but goes over the entire process of setting up a backend, including how to make an IoT device communicate with it. Following this guide basically shaved weeks off the development time (The video will not be shared here for privacy reasons). <b>In short, this means I have nothing noteworthy to say about using NGrok or MAMP, as using these wasn't too much of a kerfuffle</b>.

This did not mean everything went, as they say, <i>swimmingly</i>. The biggest issue came with doing everything somewhat neatly. It was easy to refresh the entire page using PHP, but also extremely inefficient, and not to mention very ugly. Getting only the table to update was where issues started, and then making it so that it wasn't hardcoded to my situation is what made it more difficult. <br>In short; I started out by 'echo'-ing a table into existance with the use of PHP, but I ended up generating a html string every time I retrieved the database, which works much more efficiently, and is much cleaner. That does not mean it's perfect, but it's easy to work with as an administrator, which is a must!

### Constructing API's
Retrieving data from my database was issue number two, and given that I was now able to implement it into my frontend, it was easy to test it, hence why I did it in this order (local would say the back-end comes before the front-end; function over form). I had briefly worked with API's before, so their function wasn't what bothered me. It was the JSON coding that made it difficult. Bas Pijls' tutorials made it somewhat easy, but my clunky coding habbits made converting difficult, and really was just a case of trial-and-error; removing a block of code, adding a console.log, the usual. <br>

What stood out was the issue I got later on down the line, when my frontend received an empty string of database info from my API. It took me two days to figure out my API string body was empty because of an error caused by a recently implemented <i>comment</i> I put on my API's .php file. I only learned about this after reverting to a previous version of the API .php file with Git (which I used all throughout the development).<br>

The last issue that really stood out was formatting the JSON string into arrays that I could use to generate my front-end data. This was probably the most fun, as I actually did this myself entirely. <i>I promise that this is the only code snippet I'll be boring you with</i>.

![Code Example](https://i.imgur.com/w2acQQw.png)

The function processVehicleData isn't perfect, but is the result of several itterations which used to look even more worse. My primary responsibility was to make it's workings clear to any future programmers, which is why I kept it relatively simple; the 'Table' element on my website receives a new string of html code every 5 seconds (or however long you want it to update). Within this string, data from the custom Database API (detailed in the previous heading) is implemented (see lines 119, 120, and 121). This is derived from an array that organises and extrapolates the data from the string the aforementioned database API gives us.

Yes, this isn't perfect, but it's close to perfect, and most importantly, it works!

### Some nice extra's
As I was now done with the 'must's from my MoSCoW analysis, I decided to do something more fun than solving issues to do with irrelevant comments; implementing a live map. I had heard from OpenStreetMap, but there was more documentation on something called <b>Leaflet JS</b>. Sepcifically, a very nice <a href="https://www.youtube.com/watch?v=ls_Eue1xUtY&t=1559s">video tutorial</a>, and <a href="https://leafletjs.com/SlavaUkraini/examples/quick-start/">very good documentation</a>. All this background information resulted in a relatively easy process, that (sadly for this documentation) resulted in relatively little to talk about, but I did have to add another bit of custom code for the moving marker, that made things interesting. Leaflet can't move existing markers (as far as I was able to ascertain), so I had to remove the existing markers, and replace them every update. This luckily wasn't too inefficient, so I'm not too worried about it.

### Future of the Web Application
Personally, I am very happy with the results, given that I've had to touch on every topic of web developent in some capacity, encountered new issues that helped me learn new things, and actually managed to get a capable, functioning, and effective end result that I can explain in detail. It can't all be roses, though, and it's not perfect. There is still a little bit of hard coding in there, and given that it runs off of MAMP, it can't actually be deployed in a functioning capacity yet. That part is beyond my capabilities, as well as beyond the requirements for the Internet of Things course.<br>

In short; I would have like for the site to have a more professional appeal, with accounts, logging in, and automatic setups. The use of MAMP and NGrok basically needs to be removed, but I am not the person to do this. That being said, I have learned to like web development, and how many developers are able to say that!
<hr>

## Design Thinking Poster
![DT Poster](https://i.imgur.com/XT70q42.jpg)