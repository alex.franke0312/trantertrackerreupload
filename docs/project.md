# Tranter Tracker
<strong>An IOT Project by Alexander Franke, <br>Student at the Amsterdam University of Applied Sciences.</strong><br>
<small>Contact information at the bottom of this page.</small>

## Why do we need a Tracker?
During my time as a food delivery employee, I noticed the complete isolation in communication between the restaurant in myself, with one exception; phone calls. Though effective, handling a phone while driving is dangerous and impractical. It's also often a delayed interaction, requiring a lot of conversation before information can be shared, for instance "<i>I have one delivery left before I return</i>". What if this was possible using a single button push?

## What does it do?
In short, it's an efficient, non-obtrusive communicator. In order of importance;
<ol>
<li>The Tracker can communicate how many deliveries are left, before the driver is RTB (returning to base).
<li>The Tracker tracks distance from the dispatch or headquarters.
<li>The Tracker gives insights into the effectiveness of an employee by measuring data.
<li>The Tracker is an add-on to the process; it is not meant to interfere with existing workflows.
</ol>
This is all accessed and controlled with limited inputs, allowing for safe operation during driving.

### Contents of Repository
The repository can be found <a href="https://gitlab.fdmci.hva.nl/frankea2/trantertracker">here</a>.<br>
The repository consists out of 4 folders, a mkdocs file, and a ReadMe file. The folders are;
<ul>
<li>docs - Contain the pages for MkDocs.
<li>GPSTester - A test .ino file for debugging the functionality of the GPS module.
<li>htdocs - Contains the website front- and backend files, to be ran by MAMP.
<li>TranterTracker - Contains the main .ino file for the LilyGo T-Beam.
</ul>
The mkdocs.yml file is the configuration file for MkDocs when its being ran from the console, and the ReadMe is a typical ReadMe.

### MkDocs info
This Gitlab Pages site is generated with CI/CD using MkDocs through Python 3.9-slim. The MkDocs theme used is <a href="https://github.com/cjsheets/mkdocs-rtd-dropdown">ReadTheDocs Dropdown</a>, which is implemented in the .gitlab-ci.yml file through the requirements.txt, which can be found in the root Git files. Learn how to use themes for MkDocs <a href="https://www.mkdocs.org/getting-started/#theming-our-documentation">here</a>.

## Contact Information
Alexander Franke<br>
University E-Mail: <a href="mailto:alex.franke@hva.nl">alex.franke@hva.nl</a><br>
Personal E-Mail: <a href="alex.franke0312@gmail.com">alex.franke0312@gmail.com</a><br>
Student No. 500792174<br>

<small>Last update: April 13<sup>th</sup>, 2022</small>