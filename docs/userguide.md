## User guide for TranterTracker
This user guide assumes that the TranterTracker has been installed in your fleet's vehicles. Starting everything up is relatively easy.<br>
<strong>This set-up method is not ideal, and the main point of development right now!</strong>

### Set-up
<ol>
    <li>Check if the TranterTracker device is plugged into the 12v power socket of your vehicle. If the device is 'cold' (hasn't been used for more than 30 minutes), it might take a couple of minutes for the GPS to kick in.
    <li>Connect the TranterTracker to a Hotspot of the driver of the vehicle. To do this, configure the hotspot to have the name "TranterTrackerHS", and password "TTHSsetup123". The device is now running and connected.
    <li>Start up the MAMP software, and wait until both servers (Apache and MySQL) are running.
    <li>Open your browser (any will do, except Internet Explorer) and head to "http://localhost/TrackerWebsite.php".
</ol>

### Operation
<ul>
    <li> The TranterTracker has 4 buttons that do the same thing; communicate a number back to the restaurant. Holding down one of these buttons for 2 seconds toggles it.
    <li> The number toggled should correspond to the amount of deliveries remaining, and will look like this on the Web interface;
</ul>
![Example Status WebApp](https://i.imgur.com/mE8mccN.png)


### Known issues and fixes
If the GPS is connected, but the site is not reliably updating the data, the problem might stem from the internet connection. Make sure the hotspot is configured correctly.

Depending on placement of the device, getting a connection might take up to 1 minute. The recommended positioning would be on the dashboard, with as little metal or other material between the sky and the device as possible.

<i>Note: The TranterTracker is still undergoing heavy development, meaning that some of these instructions might change over time. Last update: 23<sup>st</sup> of March, 2022</i>