# Welcome to TranterTracker
<strong>An IOT Project by Alexander Franke, <br>Student at the Amsterdam University of Applied Sciences.</strong><br>
<small>Contact information at the bottom of this page.</small>

## What is the Tranter Tracker?
<small>First off: The meaning of 'To Trant' comes from Middle English 'tranten', from or cognate with Middle Dutch 'tranten' ("to step, walk"), and loosely translates to traveling, which is basically what food delivery services are all about, right?</small>

<strong>Besides a far-fetched name, the Tranter Tracker is a service consisting out of a webpage, and a set of mobile devices that run on 12v car power, that use internet to communicate with aforementioned webpage. The goal behind the Tranter Tracker is to allow employers in the food delivery service branch more insights into their delivery employees.</strong>

## Index
On this site you'll find all relevant information is high detail, spread out over several pages.<br>
The <strong>Project Overview</strong> will go over the basic functionality and selling point of the TranterTracker. Here is where you'll find the elevator pitch.<br>
The <strong>User Guide</strong> goes over how the end user has to set up the device in its current state.<br>
The <strong>Admin guide</strong> is for the referencing of any technical administrators using this product, and should be your starting point if you want to contribute or otherwise add to the TranterTracker's development.<br>
The <strong>Technical details</strong> contains all information of the hard- and software, and breaks down how the TranterTracker works on a fundamental level.<br>
The <strong>Project course Documentation</strong> details all the steps taken along the development of the TranterTracker, in accordance with the requirements of the Internet of Things course, which served as the spark of this product.<br>

The repository can be found <a href="https://gitlab.fdmci.hva.nl/frankea2/trantertracker">here</a>.

## What does it look like
<img alt="Website Example" src="https://imgur.com/WAQ3NbU.png">

<h2>Contact Information</h2>
Alexander Franke<br>
University E-Mail: <a href="mailto:alex.franke@hva.nl">alex.franke@hva.nl</a><br>
Personal E-Mail: <a href="alex.franke0312@gmail.com">alex.franke0312@gmail.com</a><br>
Student No. 500792174<br>

<small>Last update: April 11<sup>th</sup>, 2022</small>