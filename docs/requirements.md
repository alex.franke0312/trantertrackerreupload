## Project Requirements
As this is a product developed for educational purposes paralel to the methods of the Amsterdam University of Applied Science's HBO-ICT Bachelor program, the product contains requirements that it has to adhere to. This is to ensure that not only something is created, but something is learned. This page will go into depth on the requirements of the three sections of the project.

All sections have a scaling grade, meaning that higher grades have higher requirements. In this page, we will differentiate between the passing grade (minimum requirements), and the highest scoring grade.
<hr>
### Embedded Technologies
<b>Minimum requirements, technical:</b>
<ul>
    <li>At least one way to communicate the state of the application to the user (By using light, sound or movement).
    <li>It has to react to user input (using sensors and/or buttons).
    <li>It sends data through the internet to a remote server.
    <li>It receives data through the internet on a remote server.
</ul>

<b>Minimum requirements, documental:</b>
<ul>
    <li>User Guide; A guide to help the end-user to set-up the product in order to use it.
    <li>Admin Guide; A guide to help an informed user or administrator to set-up the device so it is ready for an end-user to use it. 
    <li>Architecture overview; A schematic overview of the components that make up the product and how they are connected. 
    <li>Wiring Diagram; A schematic overview of the hardware-components and how they are connected. Unexperienced students can use Fritzing, experienced users are required to draw a schematic in an EDA of their choice (EagleCAD, KiCAD, EasyEDA, etc.)  
    <li>Bill of Materials; A list of hardware components used in your product. 
    <li>Development Process; A description of your plan (or steps taken) during the development, resources used (tutorials, guides, examples etc.) and what you learned (new knowledge and skills acquired) during this part of the individual project. 
</ul>

<b>Highest scoring grade:</b>
<ul>
    <li>All technologies are present.
    <li>At least two of the chosen technologies used surpass the level of the classroom assignments.
    <li>All the technologies are of professional quality; The device works flawlessly, is made permanent by milling a PCB and soldering, and code is of professional quality (well commented, clear variable names).
    <li>Object Oriented techniques used whenever necessary.
    <li>The documentation is professional enough to be bundled with a sold product and is tested with the target audience (An average user can set-up the product, and an educated user can set-up the product without help from the developer).
</ul>
<hr>
### Web Application Development
<b>Minimum requirements, technical:</b>
<ul>
    <li>A back-end (server component) used by your front-end and embedded device for sending, retrieving and storing data (also part of requirements of embedded technology). 
    <li>A front-end (website) which displays data and interacts with the embedded device through the back-end. 
    <li>An integrated external API that is relevant for your project (ie. Google Maps, Weather Service, Public Transport etc.). 
</ul>

<b>Minimum requirements, documental:</b>
<ul>
    <li>Architectural overview; An overview of components in your web application and how they communicate and interrelate.  
    <li>Functional design; A detailed description of how the functionality works (or is supposed to work). For example (use case) scenarios, flow chart (from the user perspective), wireframes, screenshots.
    <li>Technical design; A detailed description of the web application that describes design choices, libraries or frameworks that were used. For example UML (class diagrams, sequence diagrams), flow chart (technical perspective), API documentation, ERD. 
    <li>Development Process; A description of your plan (or steps taken) during the development, resources used (tutorials, guides, examples etc. with proper referencing (APA or IEEE)) and describes what you learned (new knowledge and skills acquired) during this part of the individual project.
</ul>

<b>Highest scoring grade:</b>
<ul>
    <li>Deployed to a 'real' hosting environment 
    <li>Professional development tools used (ie. gitlab/hub, continuous integration, docker) 
    <li>Elaborate use of front-end or back-end frameworks 
    <li>Front end is a proper single page application 
    <li>Security by design 
    <li>Coding standards and automation of code quality 
    <li>Professional documentation practices (ie. instructions for use, standards) 
    <li>You provided compelling evidence of learning something new. 
</ul>
<hr>
### Design Thinking
<b>Minimum requirements:</b>
<ul>
    <li>Problem statement 
    <li>Explication of Cultural Probe 
    <li>Persona (with readable needs and goals) 
    <li>Research data 
    <li>Scenario 
    <li>Conclusion
    <li>Poster has a title, your name and student number on it, if not: no grading. 
    <li>Poster is printed in A2 format, full color. 
    <li>Any text in poster is effortlessly readable 
</ul>

<b>Highest scoring grade:</b>
<ul>
    <li>A lot of desk and/or field research has gone into problem statement and is accounted for, prior to probe.
    <li>Varied appropriate and multiple tasks during a longer term. Behavioural patterns and insights can be derived from the tasks. 
    <li>The info about the persona is based on own field research evidence and matches the research population.
    <li>Research population exceeds 25 people and is well described and the data has been statistically analyzed.
    <li>Both scenario and storyboard contain detailed interactions (including input - output moments of the user and the system, such as notifications, feedback, pop ups) that can be linked to the persona’s goals.
    <li>Conclusion answers the question whether or not your IOT artefact meets the users needs and gives validated reasons based on extensive research (minimum requirement for this is intermediate on user research and research data).
</ul>
<hr>
### Methods & Technologies exam
Next to the three sections mentioned, there is the personal matter of resitting the Methods & Technologies exam for my work on the Little Endian. For this, requirements are somewhat simpler, but require a lot of work. For organisational purposes, these will be placed here as well.

<b>Minimum requirements:</b>
<ul>
    <li>Professional Task Form, containing the following; 
    <ul>
        <li>1 large task (66% of the project time).
        <li>2 smaller tasks done during the project (each roughly 10% of project time).
        <li>All tasks should contain explanations and proof (You proof can be anything, ranging from a stakeholder analysis to a research document to a piece of (documented) code).
    </ul>
    <li>Lobook of activities and hours worked on the task(s).
</ul>